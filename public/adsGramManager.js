var adsGramController;

function initAdsGramController(blockId) {
    adsGramController = window.Adsgram.init({blockId: blockId});
}

function showSkipTimeAd() {
    adsGramController.show().then((result) => {
        setTimeout(() => unityInstance.SendMessage('AdsManager', 'AdSuccess', 'skipTime'), 500);
    }).catch((result) => {
        unityInstance.SendMessage('AdsManager', 'AdFail', 'skipTime');
    });
}

