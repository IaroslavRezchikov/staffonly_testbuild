var tadsController;
var currentId;
var container;
var searchIntervalId = null; // Добавляем переменную для хранения ID интервала

const initTadsWidget = (id, debug, onShowReward, onClickReward, onAdsNotFound, x1, y1, x2, y2) => {
    tadsController = window.tads.init({widgetId: id, debug: debug, onShowReward, onClickReward, onAdsNotFound});
    currentId = id;

    tadsController.loadAd()
        .then(() => {
            if(tadsController){
                tadsController.showAd();
                UpdateWidgetPosition(x1, y1, x2, y2);
            }
        })
        .catch((error) => {
            console.error('Error loading ad:', error);
            stopButtonSearch(); // Останавливаем поиск кнопки перед вызовом onAdsNotFound
            onAdsNotFound();
        });
};

function showTadsAdWithId(id, x1, y1, x2, y2) {
    if ([x1, y1, x2, y2].some(val => val == null)) {
        console.error('Invalid widget position parameters:', x1, y1, x2, y2);
        return;
    }

    initTadsWidget(id, false, onShowRewardCallback, onClickRewardCallback, onAdsNotFound, x1, y1, x2, y2);
}

function destroyTadsWidget(){
    stopButtonSearch(); // Останавливаем поиск кнопки перед уничтожением виджета

    if(tadsController){
        tadsController.destroyAd();
        tadsController = null;
    }
    if(container){
        container.style.width = '0';
        container.style.height = '0';
        container.style.left = '-9999px';
        container.style.top = '-9999px';
    }
}

// Новая функция для остановки поиска кнопки
function stopButtonSearch() {
    if (searchIntervalId !== null) {
        clearInterval(searchIntervalId);
        searchIntervalId = null;
    }
}

function UpdateWidgetPosition(x1, y1, x2, y2) {
    container = document.querySelector(`#tads-container-${currentId}`);
    let button = document.querySelector('button.tads');

    if (!container) {
        console.error('Ad container not found');
        return;
    }

    if (!button) {
        searchIntervalId = setInterval(() => {
            button = document.querySelector('button.tads');
            if (button) {
                stopButtonSearch();
                updatePosition();
            }
        }, 100);
    } else {
        updatePosition();
    }

    function updatePosition() {
        const canvas = document.querySelector("#unity-canvas");
        if (!canvas) {
            console.error('Unity canvas not found');
            return;
        }

        const rect = canvas.getBoundingClientRect();
        const scaleX = rect.width / canvas.width;
        const scaleY = rect.height / canvas.height;

        const left = rect.left + x1 * scaleX;
        const top = rect.top + rect.height - (y2 * scaleY);
        const width = (x2 - x1) * scaleX;
        const height = (y2 - y1) * scaleY;

        container.style.position = 'absolute';
        container.style.left = `${left}px`;
        container.style.top = `${top}px`;
        container.style.width = `${width}px`;
        container.style.height = `${height}px`;
        container.style.zIndex = 9999;

        button.style.position = 'absolute';
        button.style.left = '0';
        button.style.top = '0';
        button.style.width = '100%';
        button.style.height = '100%';
        button.style.display = 'block';

        button.style.backgroundColor = 'transparent';
        button.style.border = 'none';
        button.style.boxShadow = 'none';

        setTimeout(() => {
            button.childNodes.forEach(child => {
                if (child.nodeType === Node.ELEMENT_NODE) {
                    child.style.opacity = '0';
                    child.style.height = '0';
                    child.style.pointerEvents = 'none';
                    child.style.userSelect = 'none';
                    child.setAttribute('tabindex', '-1');
                }
            });
        }, 100);

        unityInstance.SendMessage('AdsManager', 'AdWidgetShown', currentId);
    }
}

// Callbacks
const onShowRewardCallback = (result) => {
};

const onClickRewardCallback = (result) => {
    unityInstance.SendMessage('AdsManager', 'AdSuccess', currentId);
    destroyTadsWidget();
};

const onAdsNotFound = () => {
    unityInstance.SendMessage('AdsManager', 'AdFail', currentId);
    destroyTadsWidget();
};
